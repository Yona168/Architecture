package com.gitlab.avelyn.architecture.utilites;

import java.util.List;
import java.util.function.BiConsumer;
import java.util.function.Consumer;

public interface Event<Listener> {
	List<Listener> getListeners();
	
	default Listener run(Listener listener) {
		getListeners().add(listener);
		return listener;
	}
	
	static <First, Second> void fire(Event<BiConsumer<First, Second>> event, First first, Second second) {
		List<BiConsumer<First, Second>> listeners = event.getListeners();
		for (int i = 0; i < listeners.size(); i++)
			listeners.get(i).accept(first, second);
	}
	
	static <Type> void fire(Event<Consumer<Type>> event, Type type) {
		List<Consumer<Type>> listeners = event.getListeners();
		for (int i = 0; i < listeners.size(); i++)
			listeners.get(i).accept(type);
	}
	
	static void fire(Event<Runnable> event) {
		List<Runnable> listeners = event.getListeners();
		for (int i = 0; i < listeners.size(); i++)
			listeners.get(i).run();
	}
}
