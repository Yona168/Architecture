package com.gitlab.avelyn.architecture.utilites;

import com.gitlab.avelyn.architecture.base.Component;
import com.gitlab.avelyn.architecture.base.Parent;
import com.gitlab.avelyn.architecture.base.Phase;
import com.gitlab.avelyn.architecture.base.Toggleable;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public final class Components {
	
	private Components() {
	}
	
	public static <Type> Component addTo(Collection<Type> list, Type element) {
		return component(() -> list.add(element), () -> list.remove(element));
	}
	
	public static <Type> Component addToList(List<Type> list, Type element, int index) {
		return component(() -> list.add(index, element), () -> list.remove(element));
	}
	
	public static List<Toggleable> getChildrenRecurse(Parent<Toggleable> component) {
		return getChildrenRecurse(component.getChildren());
	}
	
	private static List<Toggleable> getChildrenRecurse(List<Toggleable> children) {
		final List<Toggleable> deepChildren = new ArrayList<>();
		for (Toggleable child : children)
			if (child instanceof Component)
				deepChildren.addAll(getChildrenRecurse(((Component) child).getChildren()));
		children.addAll(deepChildren);
		return children;
	}
	
	public static Phase phase(Runnable enable) {
		return new Phase()
				.onEnable(enable);
	}
	
	public static Phase phase(Runnable enable, Runnable disable) {
		return new Phase()
				.onEnable(enable)
				.onDisable(disable);
	}
	
	public static Phase phase(Runnable enable, Runnable disable, Runnable complete) {
		return new Phase()
				.onEnable(enable)
				.onDisable(disable)
				.onComplete(complete);
	}
	
	public static Component component(Runnable enable) {
		return new Component()
				.onEnable(enable);
	}
	
	public static Component component(Runnable enable, Runnable disable) {
		return new Component()
				.onEnable(enable)
				.onDisable(disable);
	}
}