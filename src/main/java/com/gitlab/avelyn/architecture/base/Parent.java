package com.gitlab.avelyn.architecture.base;


import java.util.List;

public interface Parent<Child> {
	/**
	 * @return - This {@link Parent}'s children.
	 */
	List<Child> getChildren();
	
	/**
	 * Adds the specified child to {@link #getChildren()}
	 * if not present.
	 *
	 * @param child
	 * 		- The child to add.
	 * @return - The child added.
	 */
	default <B extends Child> B addChild(B child) {
		if (!getChildren().contains(child)) {
			getChildren().add(child);
		}
		return child;
	}
	
	/**
	 * Adds the specified children to {@link #getChildren()}
	 * if not present.
	 *
	 * @param children
	 * 		- The children to add.
	 * @return - The children added.
	 */
	
	default <B extends Child> B[] addChild(B... children) {
		for (B child : children)
			addChild(child);
		return children;
	}
	
	
	/**
	 * Removes the specified child from {@link #getChildren()}
	 * if present.
	 *
	 * @param child
	 * 		The child to remove.
	 * @return {@code true} if the child was removed.
	 */
	default boolean removeChild(Child child) {
		return getChildren().remove(child);
	}
	
	/**
	 * Removes the specified children from {@link #getChildren()}
	 * if present.
	 *
	 * @param children
	 * 		The children to remove.
	 * @return {@code true} if any of the children were removed.
	 */
	default boolean removeChild(Child... children) {
		boolean result = false;
		for (Child child : children)
			result |= removeChild(child);
		return result;
	}
}